﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerState : MonoBehaviour
{
    /*public string playername;
    public string playerrace;
    public string playerclass;
    public string playeralignment;
    public int maxHP;
    public int currentHP;
    public int currentXP;
    public int maxXP;
    public int walking;
    public int running;
    public int jump;
    public int armorclass;
    */
    //public List<string> itemList;
    public ability abilities;

    [System.Serializable]
    public class ability
    {
        public string playername;
        public string playerrace;
        public string playerclass;
        public string playeralignment;
        public int maxHP;
        public int currentHP;
        public int currentXP;
        public int maxXP;
        public int walking;
        public int running;
        public int jump;
        public int armorclass;
        public int strength = 15; 
        public int dexterity = 15;
        public int wisdom = 15;
        public int charisma = 15;
        public int intelligence = 15;
        public int constitution = 15;
    }
    
    public void setPlayerName(string name)
    {
        abilities.playername = name;
    }


    public void setPlayerRace(string race)
    {
        abilities.playerrace = race;
    }

    public void setPlayerClass(string classs)
    {
        abilities.playerclass = classs;
    }

    public void setPlayerAlignment(string alignment)
    {
        abilities.playeralignment = alignment;
    }

    public void setMaxHP(int HP)
    {
        abilities.maxHP = HP;
    }

    public void setCurrentHP(int HP)
    {
        abilities.currentHP = HP;
    }


    public void setCurrentXP(int XP)
    {
        abilities.currentXP = XP;

    }

    public void setMaxXP(int XP)
    {
        abilities.maxXP = XP;
    }


    public void setWalk(int walk)
    {
        abilities.walking = walk;
    }

    public void setRun(int run)
    {
        abilities.running = run;
    }

    public void setJump(int jumping)
    {
        abilities.jump = jumping;
    }

    public void setArmorClass(int val)
    {
        abilities.armorclass = val;
    }

    public void setItemList()
    {
       // itemList = new List<string>();
    }

    public string SaveToString()
    {
        return JsonUtility.ToJson(this);
    }


    
}
