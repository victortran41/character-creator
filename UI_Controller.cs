﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class UI_Controller : MonoBehaviour
{
    public List<string> races = new List<string>() { "Dragonborn", "Dwarf", "Elf", "Gnome", "Half-Elf", "Half-Orc", "Halfling", "Human", "Tiefling" };
    public List<string> alignment = new List<string>() { "Lawful Good", "Lawful Neural", "Lawful Evil", "Neural Good", "True Neural", "Neural Evil", "Chaotic Good", "Chaotic Neural", "Chaotic Evil" };
    public List<string> classes = new List<string>() { "Barbarian", "Bard", "Cleric", "Druid", "Fighter", "Monk", "Paladin", "Ranger", "Rogue", "Sorcerer", "Warlock", "Wizard" };
    public Text strength;
    public Text dexterity;
    public Text wisdom;
    public Text constitution;
    public Text intelligence;
    public Text charisma;
    public Dropdown race_dropdown;
    public Dropdown class_dropdown;
    public Dropdown alignment_dropdown;
    public Text JSON_Output;
    public InputField input_field;
    public InputField current_HP;
    public InputField max_HP;
    public InputField current_XP;
    public InputField max_XP;
    public InputField walking;
    public InputField running;
    public InputField jump;
    public InputField armor_class;
    private PlayerState PS;

    public void Roll_Dice()
    {
        int[] abilityScores = new int[6];

        for (int j = 0; j < 6; j++) {
            List<int> rolled_list = new List<int>();
            int dice1 = UnityEngine.Random.Range(1, 7);
            int dice2 = UnityEngine.Random.Range(1, 7);
            int dice3 = UnityEngine.Random.Range(1, 7);
            int dice4 = UnityEngine.Random.Range(1, 7);
            int dice5 = UnityEngine.Random.Range(1, 7);
            rolled_list.Add(dice1);
            rolled_list.Add(dice2);
            rolled_list.Add(dice3);
            rolled_list.Add(dice4);
            rolled_list.Add(dice5);
            rolled_list.Sort();
            rolled_list.Reverse();
            int total = rolled_list[0] + rolled_list[1] + rolled_list[2];
            abilityScores[j] = total;
        }
        strength.text = abilityScores[0].ToString();
        dexterity.text = abilityScores[1].ToString();
        wisdom.text = abilityScores[2].ToString();
        constitution.text = abilityScores[3].ToString();
        intelligence.text = abilityScores[4].ToString();
        charisma.text = abilityScores[5].ToString();
        Store_PSAbilities();

    }

    public void FetchValues()
    {

        int maxHP = 0;
        int currentHP = 0;
        int maxXP = 0;
        int currentXP = 0;
        int walkingVal = 0;
        int runningVal = 0;
        int jumpVal = 0;
        int ArmorClassVal = 0;

        if (max_HP.text.Equals(""))
        {
            maxHP = 0;
        }
        else
        {
            maxHP = Convert.ToInt32(max_HP.text);
        }
        if (current_HP.text.Equals(""))
        {
            currentHP = 0;
        }
        else
        {
            currentHP = Convert.ToInt32(current_HP.text);
        }
        if (max_XP.text.Equals(""))
        {
            maxXP = 0;
        }
        else
        {
            maxXP = Convert.ToInt32(max_XP.text);
        }
        if (current_XP.text.Equals(""))
        {
            currentXP = 0;
        }
        else
        {
            currentXP = Convert.ToInt32(current_XP.text);
        }
        if (walking.text.Equals(""))
        {
            walkingVal = 0;
        }
        else
        {
            walkingVal = Convert.ToInt32(walking.text);
        }
        if (running.text.Equals(""))
        {
            runningVal = 0;
        }
        else
        {
            runningVal = Convert.ToInt32(running.text);
        }
        if (jump.text.Equals(""))
        {
            jumpVal = 0;
        }
        else
        {     
            jumpVal = Convert.ToInt32(jump.text);
        }
        if (armor_class.text.Equals(""))
        {
            ArmorClassVal = 0;
        }
        else
        {
            ArmorClassVal = Convert.ToInt32(armor_class.text);
        }

    }

    public void Store_PSAbilities()
    {
        FetchValues();

        PS.abilities.playername = input_field.text;
        PS.abilities.playerrace = race_dropdown.options[race_dropdown.value].text;
        PS.abilities.playerclass = class_dropdown.options[class_dropdown.value].text;
        PS.abilities.playeralignment = alignment_dropdown.options[alignment_dropdown.value].text;
        /*
        PS.abilities.maxHP = Convert.ToInt32(max_HP.text);
        PS.abilities.currentHP = Convert.ToInt32(current_HP.text);
        PS.abilities.currentXP = Convert.ToInt32(current_XP.text);
        PS.abilities.maxXP = Convert.ToInt32(max_XP.text);
        PS.abilities.walking = Convert.ToInt32(walking.text);
        PS.abilities.running = Convert.ToInt32(running.text);
        PS.abilities.jump = Convert.ToInt32(jump.text);
        PS.abilities.armorclass = Convert.ToInt32(armor_class.text);

        PS.abilities.strength = Convert.ToInt32(strength.text);
        PS.abilities.wisdom = Convert.ToInt32(wisdom.text);
        PS.abilities.dexterity = Convert.ToInt32(dexterity.text);
        PS.abilities.charisma = Convert.ToInt32(charisma.text);
        PS.abilities.constitution = Convert.ToInt32(constitution.text);
        PS.abilities.intelligence = Convert.ToInt32(intelligence.text);
        */

    }

    public void savePS()
    {
        print("hahaha " + max_HP.text);
        int maxHP = Convert.ToInt32(max_HP.text);
        int currentHP = Convert.ToInt32(current_HP.text);
        
        int maxXP = Convert.ToInt32(max_XP.text);
        int currentXP = Convert.ToInt32(current_XP.text);

        int walkingVal = Convert.ToInt32(walking.text);
        int runningVal = Convert.ToInt32(running.text);
        int jumpVal = Convert.ToInt32(jump.text);
        int ArmorClassVal = Convert.ToInt32(armor_class.text);



        PS.setPlayerRace(race_dropdown.options[race_dropdown.value].text);
        PS.setPlayerClass(class_dropdown.options[class_dropdown.value].text);
        PS.setPlayerAlignment(alignment_dropdown.options[alignment_dropdown.value].text);
        PS.setPlayerName(input_field.text);
        PS.setMaxHP(Convert.ToInt32(max_HP.text));
        PS.setCurrentHP(Convert.ToInt32(current_HP.text));
        PS.setCurrentXP(Convert.ToInt32(current_XP.text));
        PS.setMaxXP(Convert.ToInt32(max_XP.text));
        PS.setWalk(Convert.ToInt32(walking.text));
        PS.setRun(Convert.ToInt32(running.text));
        PS.setJump(Convert.ToInt32(jump.text));
        PS.setArmorClass(Convert.ToInt32(armor_class.text));
        PS.setItemList();

    }

    public void makeJSON()
    {
        JSON_Output.text = PS.SaveToString();
        Debug.Log(JSON_Output.text);
    }

    public void switchToMain(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void switchToMainMenu(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void quitGame()
    {
        Application.Quit();
        Debug.Log("Game has ended");
    }
 


    // Start is called before the first frame update
    void Awake()
    {
        PS = new PlayerState();
        PS.abilities = new PlayerState.ability();
        race_dropdown.AddOptions(races);
        alignment_dropdown.AddOptions(alignment);
        class_dropdown.AddOptions(classes);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
